package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.CurrencyModel;

import java.util.Optional;

public interface CurrencyService {

    Optional<CurrencyModel> findByName(String name);

    CurrencyModel save(CurrencyModel model);
}
