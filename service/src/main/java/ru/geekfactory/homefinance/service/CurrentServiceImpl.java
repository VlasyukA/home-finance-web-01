package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.repository.ConnectionSupplier;
import ru.geekfactory.homefinance.dao.repository.CurrencyRepository;
import ru.geekfactory.homefinance.dao.repository.Repository;

import java.util.Optional;

public class CurrentServiceImpl implements CurrencyService {

    @Override
    public Optional<CurrencyModel> findByName(String name) {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        CurrencyRepository currencyRepository = new CurrencyRepository(connectionSupplier);

        return currencyRepository.findByName(name);
    }

    @Override
    public CurrencyModel save(CurrencyModel model) {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        Repository<CurrencyModel, Long> currencyRepository = new CurrencyRepository(connectionSupplier);

        return currencyRepository.save(model);
    }
}
