package ru.geekfactory;

import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.repository.ConnectionSupplier;
import ru.geekfactory.homefinance.dao.repository.CurrencyRepository;

import java.sql.*;

public class Main {

    private final static String DB_URL = "jdbc:mysql://localhost:3306/home_finance?useSSL=false&serverTimezone=UTC";
    private final static String DB_USER = "root";
    private final static String DB_PASSWORD = "secret";

    private final static String FIND_ALL = "SELECT * FROM currency_tbl";

    public static void main(String[] args) {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        CurrencyRepository currencyRepository = new CurrencyRepository(connectionSupplier);

        CurrencyModel currencyModel = new CurrencyModel();
        currencyModel.setName("Евро");

        System.out.println("Before save currencyModel = " + currencyModel);
        currencyRepository.save(currencyModel);
        System.out.println("After save currencyModel = " + currencyModel);
    }

    private static void test() throws SQLException {
        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL)) {

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    long id = resultSet.getLong("id");
                    String name = resultSet.getString("name");
                    System.out.println("id = " + id + " name = " + name);
                }
            }

        }
    }
}
