package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;

import java.sql.*;
import java.util.Collection;
import java.util.Optional;

public class CurrencyRepository implements Repository<CurrencyModel, Long> {
    private static final String INSERT = "INSERT INTO currency_tbl (name, code, symbol) VALUES (?, ?, ?)";
    private static final String SELECT_BY_NAME = "SELECT * FROM currency_tbl WHERE name = ?";
    private ConnectionSupplier connectionSupplier;

    public CurrencyRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public CurrencyModel save(CurrencyModel model) {

        try (Connection connection = connectionSupplier.getConnection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, model.getName());
                preparedStatement.setString(2, model.getCode());
                preparedStatement.setString(3, model.getSymbol());
                preparedStatement.executeUpdate();

                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }

                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceDaoException("error while save currency model " + model, e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("error while save currency model " + model, e);
        }
    }

    @Override
    public Optional<CurrencyModel> findByName(String name) {

        try (Connection connection = connectionSupplier.getConnection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_NAME, Statement.RETURN_GENERATED_KEYS)){
                preparedStatement.setString(1, name);

                ResultSet resultSet = preparedStatement.executeQuery();
                CurrencyModel currencyModel = null;

                while (resultSet.next()) {
                    String currentName = resultSet.getString("name");
                    String code = resultSet.getString("code");
                    String symbol = resultSet.getString("symbol");

                    currencyModel = new CurrencyModel(currentName, code, symbol);
                }

                return Optional.ofNullable(currencyModel);
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceDaoException("error while find currency model by name " + name, e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("error while find currency model by name " + name , e);
        }
    }

    @Override
    public Collection<CurrencyModel> findAll() {
        return null;
    }

    @Override
    public boolean remove(Long aLong) {
        return false;
    }

    @Override
    public CurrencyModel update(CurrencyModel model) {
        return null;
    }
}
