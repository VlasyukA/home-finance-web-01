package ru.geekfactory.homefinance.dao.model;

public enum AccountType {
    CASH, DEBIT_CARD, CREDIT_CARD, DEPOSIT;
}
