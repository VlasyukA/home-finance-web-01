package ru.geekfactory.homefinance.dao.model;

import java.math.BigDecimal;

public class AccountModel {
    private long id;
    private CurrencyModel currencyModel;
    private String name;
    private AccountType accountType;
    private BigDecimal amount;
}
