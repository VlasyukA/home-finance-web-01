package ru.geekfactory.homefinance.dao.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdModel {
    private long id;
}
