package ru.geekfactory.console;

import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.service.CurrencyService;
import ru.geekfactory.homefinance.service.CurrentServiceImpl;

import java.util.Optional;
import java.util.Scanner;

import static ru.geekfactory.console.Command.EXIT;

public class Starter {
    public static void main(String[] args) {
        boolean isEnable = true;

        while (isEnable) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Select the command \n" +
                    Command.SAVE_CURRENCY.name() + "\n" +
                    Command.GET_CURRENCY.name() + "\n" +
                    EXIT.name());

            String scannerText = scanner.nextLine();
            Command command = Command.valueOf(scannerText);

            CurrencyService currencyService = new CurrentServiceImpl();

            if (EXIT.compareTo(command) == 0) {
                isEnable = false;
            }

            switch (command) {
                case GET_CURRENCY:
                    System.out.println("Enter your currency name. e. g. [Russian ruble]");
                    String name = scanner.nextLine();
                    Optional<CurrencyModel> byName = currencyService.findByName(name);
                    System.out.println(byName.get());

                    break;
                case SAVE_CURRENCY:
                    System.out.println("Enter your currency information. e. g. [Russian ruble:RUB:\u20BD]");
                    String next = scanner.nextLine();
                    String[] currencyContent = next.split(":");
                    CurrencyModel currencyModel = new CurrencyModel(currencyContent[0], currencyContent[1], currencyContent[2]);
                    currencyService.save(currencyModel);

                    break;
                default:
                    System.exit(0);
            }
        }
    }
}
