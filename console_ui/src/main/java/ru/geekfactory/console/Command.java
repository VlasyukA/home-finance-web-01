package ru.geekfactory.console;

public enum Command {
    EXIT,
    SAVE_CURRENCY,
    GET_CURRENCY
}
